#!/usr/bin/env coffee
# Server for Encrypted Email Example by Brendan Long <self@brendanlong.com>
#
# This code is in the public domain. See LICENSE or http://unlicense.org/ for
# details.
express = require 'express'
users = require './users'

app = express()
app.use express.logger()
app.use express.compress()
app.use express.bodyParser()

app.put '/users/register/:email', (request, response) ->
  params = request.body
  users.register request.params.email, params.loginKey, params.publicKey,
      params.encryptedKey, params.hmac, params.salt, params.iv,
      params.iterations, response.send.bind(response)

app.get '/users/login-info/:email', (request, response) ->
  users.getLoginInfo request.params.email, response.send.bind(response)

app.get '/users/private-key/:email', (request, response) ->
  users.getPrivateKey request.params.email, request.query.loginKey, \
      response.send.bind(response)

port = process.env.PORT or 5000
app.listen port, ->
  console.log "Listening on " + port
