"""
Security functions for Encrypted Email Example
by Brendan Long <self@brendanlong.com>

This code is in the public domain. See LICENSE or http://unlicense.org/ for
details.
"""
import Crypto

from Crypto.Cipher import AES
from Crypto.Hash import HMAC, SHA256
from Crypto.Protocol.KDF import PBKDF2
from Crypto.PublicKey import RSA
from Crypto import Random


def make_aes_keys(password, salt=None, iterations=10000):
    """
    Generates three 128-bit keys from the given password using
    PBKDF2-SHA512.

    We use PBKDF2-SHA512 because we want the native output of PBKDF2 to be
    at least 384 bits. If we stayed at the default of PBKDF2-SHA1, then the
    entire algorithm would run three times, which is slow for normal users,
    but doesn't slow things down for attackers.

       password - The password.
       salt - The salt to use. If not given, one will be generated.
       iterations - The number of iterations of PBKDF2.

    Returns (k1, k2, k3, salt, interations)
    """
    if salt is None:
        # Generate a cryptographically random salt
        salt = Random.new().read(8)

    prf = lambda p,s: HMAC.new(p, s, SHA512).digest()
    key = PBKDF2(password, salt, 48, iterations)

    return key[:16], key[16:32], key[32:], salt, iterations


def make_rsa_key(bits=3328):
    """
    Generates a new RSA key pair.

       bits - The length of the key in bits. Must be >= 1024 and divisible by
              256. 3248 bits should provide the same level of security as a
              128-bit AES key, and we round up to 3328 to be divisible by 256.

    Returns The new RSA key pair.
    """
    return RSA.generate(bits)


def load_rsa_key(raw_key):
    """
    Loads an RSA key pair from DER format.

       raw_key - The DER encoded key.

    Returns The RSA key pair.
    """
    return RSA.importKey(raw_key)


def encrypt(data, encryption_key, hmac_key):
    """
    Encrypts the given data with the given key, using AES-CFB, and
    generates an HMAC to verify it.

       message - The message to encrypt (byte string).
       encryption_key - The AES key (16 bytes).
       hmac_key - The HMAC key (16 bytes).

    Returns (ciphertext, iv, hmac). All three values are byte strings.
    """
    # The IV should always be random
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(encryption_key, AES.MODE_CFB, iv)
    ciphertext = cipher.encrypt(data)

    hmac = HMAC.new(hmac_key)
    hmac.update(ciphertext)

    return ciphertext, iv, hmac.digest()


def decrypt(data, encryption_key, iv, hmac, hmac_key):
    """
    Verifies the HMAC and then decrypts the data, using AES-CFB.

       data - The data to decrypt (byte string).
       key - The AES key (16 bytes).
       iv - The original IV used for encryption.
       hmac - The HMAC for verification.
       hmac_key - The key for the HMAC.

    Returns the cleartext (byte string). Throws an exception if the HMAC does
    not match.
    """
    # Verify the HMAC first, since there's no point to decrypting if the
    # data is invalid.
    h = HMAC.new(hmac_key)
    h.update(data)
    calculated_hmac = h.digest()
    if calculated_hmac != hmac:
        raise Exception( "Calculated HMAC {} does not match given HMAC {}."
                .format(calculated_hmac, hmac))
    
    cipher = AES.new(encryption_key, AES.MODE_CFB, iv)
    out = cipher.decrypt(data)
    return out
