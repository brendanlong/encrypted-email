#!/bin/bash -e

if [ ! -f bin/activate ] ; then
    virtualenv -p python3 .
fi
source bin/activate
pip install --upgrade pycrypto

echo "You probably want to 'source bin/activate' now."
