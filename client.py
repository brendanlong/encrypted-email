#!/usr/bin/env python3
"""
Encrypted Email Example by Brendan Long <self@brendanlong.com>

An example program for connecting to an email server that does not have
access to any secret information. The server only knows the keys generated
from a password, and the client never sends the password to the server.

This code is in the public domain. See LICENSE or http://unlicense.org/ for
details.
"""
import argparse
import base64
import getpass
import http.client
import json
import sys
import urllib

import security


LOGIN_INFO_PATH = '/users/login-info'
PRIVATE_KEY_PATH = '/users/private-key'
REGISTER_PATH = '/users/register'


class EmailClient:
    """
    An example of an email client where the server doesn't have to be
    trusted with any secret information. The user's private key is
    AES encrypted, and the login uses a different AES key (generated from
    the same password with PBKDF2).
    """

    def __init__(self, server):
        """
        Setup the EmailClient.

            server - The URL of the server to connect to.
        """
        self.server = server

    def login(self, email, password, register=True):
        """
        Attempt to log in. If the email address does not exist, and register
        is True, we will register an account instead.

           email - The user's email address.
           password - The user's password (used for generating keys -- will not
                      be sent to the server).
           register - If True, we will register a new account if it does not
                      exist.
        """
        self.email = email

        client = http.client.HTTPConnection(self.server)
        client.request('GET', '{}/{}'.format(LOGIN_INFO_PATH, email))
        response = client.getresponse()
        if response.status == 404:
            print('No such user, registering...')
            self.register(email, password)
            return
        elif response.status != 200:
            raise Exception(response.read())
        
        data = json.loads(response.read().decode('UTF-8'))
        salt = base64.b64decode(data['salt'])
        login_key, hmac_key, encryption_key, _, _ = \
                security.make_aes_keys(password, salt, data['iterations'])
        encoded_login_key = base64.b64encode(login_key).decode('UTF-8')
        client.request('GET', '{}/{}?loginKey={}'.format(PRIVATE_KEY_PATH,
                email, encoded_login_key))

        response = client.getresponse()
        if response.status != 200:
            raise Exception(response.read())
        data = json.loads(response.read().decode('UTF-8'))
        for key, value in data.items():
            data[key] = base64.b64decode(value)
        encrypted_key = data['encryptedKey']
        raw_key = security.decrypt(encrypted_key, encryption_key, data['iv'],
                data['hmac'], hmac_key)
        self.rsa_key = security.load_rsa_key(raw_key)
        print("Logged in as {}".format(self.email))

    def register(self, email, password):
        """
        Register a new account.

           email - The user's email address.
           password - The user's password (used for generating keys -- will not
                      be sent to the server).
        """
        self.email = email
        self.rsa_key = security.make_rsa_key()
        login_key, hmac_key, encryption_key, salt, iterations = \
                security.make_aes_keys(password, iterations=10000)
        encrypted_key, iv, hmac = security.encrypt(rsa_key.exportKey('DER'),
                encryption_key, hmac_key)
        public_key = rsa_key.publickey().exportKey('DER')
        
        client = http.client.HTTPConnection(self.server)
        params = { 'iterations': iterations }
        for key, value in (('loginKey', login_key), ('publicKey', public_key),
                ('encryptedKey', encrypted_key), ('iv', iv), ('salt', salt),
                ('hmac', hmac)):
            params[key] = base64.b64encode(value).decode('UTF-8')
        json_params = json.dumps(params)
        headers = {'Content-Type': 'application/json'}
        client.request('PUT', '{}/{}'.format(REGISTER_PATH, email), json_params,
                headers)

        response = client.getresponse()
        if response.status != 200:
            raise Exception(response.read())
        print("Registered email {}".format(email))


def main():
    parser = argparse.ArgumentParser(description="Client program for encrypted email example.")
    parser.add_argument('--server', '-s', default='localhost:5000',
            help='The server to connect to. Defaults to localhost:5000.')
    parser.add_argument('--email', '-e', help='Your email address. If not given, you will be prompted.')
    parser.add_argument('--password', '-p', help='Your password. Warning: This can be stored in your shell history! If not given, you will be prompted.')
    args = parser.parse_args()
    
    email = args.email or input('Email: ')
    password = args.password or getpass.getpass()

    client = EmailClient(args.server)
    client.login(email, password)


if __name__ == '__main__':
    sys.exit(main())
