# Encrypted Email Example

By [Brendan Long](mailto:self@brendanlong.com)

## Setup

### Server

To setup the server, use `npm`. You may need to install Node.js and npm first.

    :::bash
    # Fedora
    sudo yum install nodejs

    # Debian and Ubuntu
    sudo apt-get install nodejs

    npm install

### Client

To setup the client, use `setup-client.sh`. You may need to install Python 3
and virtualenv first.

    :::bash
    # Fedora
    sudo yum install python3 python-virtualenv

    # Debian and Ubuntu
    sudo apt-get install python3 python-virtualenv

    ./setup-client.sh

## Running

### Server

The server's only option currently is the port, which is determined by the
environment variable `PORT` (default = 5000).

Run like so:

    :::bash
    ./server.coffee # or `coffee server.coffee`

Or on port `8080`:

    :::bash
    PORT=8080 ./server.coffee

Currently, it uses an in-memory SQLite database, so all information will be
lost whenever it shuts down.

### Client

To see the client's options, run with `-h`:

    :::bash
    ./client.py -h # or `python3 client.py -h`
