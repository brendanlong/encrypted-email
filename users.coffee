# Server for Encrypted Email Example by Brendan Long <self@brendanlong.com>
#
# This code is in the public domain. See LICENSE or http://unlicense.org/ for
# details.
sqlite = require 'sqlite3'

EMAIL_REGEX = ///
  [a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?
///
REQUIRED_ITERATIONS = 10000


db = new sqlite.Database(':memory:')
db.run 'CREATE TABLE users (email TEXT PRIMARY KEY, loginKey BLOB, ' +
    'publicKey BLOB, encryptedKey BLOB, hmac BLOB, salt BLOB, iv BLOB, ' +
    'iterations INTEGER)'


exports.getLoginInfo = (email, callback) ->
  if not EMAIL_REGEX.test email
    callback 400, 'Email address invalid'

  db.get 'SELECT salt, iterations FROM users WHERE email = ?', [email], \
      (error, row) ->
    if error
      callback 500, "Unable to get login info due to error: #{error}"
      return
    if not row
      callback 404, 'No such user'
      return
    callback 200, { 'salt': row.salt, 'iterations': row.iterations }


exports.getPrivateKey = (email, loginKey, callback) ->
  if not EMAIL_REGEX.test email
    callback 400, 'Email address invalid'
    return

  if not loginKey
    callback 400, 'Missing parameter loginKey'
    return

  db.get 'SELECT encryptedKey, iv, hmac FROM users WHERE email = ? AND ' +
      'loginKey = ?', [email, loginKey], (error, row) ->
    if error
      callback 500, "Unable to get private key due to error: #{error}"
      return
    if not row
      callback 401, 'Either the email or loginKey given were invalid'
      return
    callback 200, { 'encryptedKey': row.encryptedKey, 'iv': row.iv, \
        'hmac': row.hmac }


# Add a user to the database if they don't already exist.
exports.register = (email, loginKey, publicKey, encryptedKey, hmac, salt, iv,
    iterations, callback) ->

  iterations = parseInt(iterations)

  # Check that we have required parameters
  for key, value of { 'email': email, 'loginKey': loginKey, \
      'publicKey': publicKey, 'encryptedKey': encryptedKey, 'hmac': hmac, \
      'salt': salt, 'iv': iv, 'iterations': iterations }
    if not value
      callback 400, "Missing parameter #{key}"
      return

  # Check that the email address is valid
  if not EMAIL_REGEX.test email
    callback 400, 'Email address invalid'
    return

  # Make sure the number of iterations is useful
  if iterations < REQUIRED_ITERATIONS
    callback 400, "Must use at least #{REQUIRED_ITERATIONS} iterations"
    return

  db.run 'INSERT INTO users (email, loginKey, publicKey, encryptedKey, ' +
      'hmac, salt, iv, iterations) VALUES (?, ?, ?, ?, ?, ?, ?, ?)', [email,
      loginKey, publicKey, encryptedKey, hmac, salt, iv, iterations], (error) ->
    if error
      callback 500, "Unable to register user due to error: #{error}"
    else
      callback 200, "User #{email} registered!"
